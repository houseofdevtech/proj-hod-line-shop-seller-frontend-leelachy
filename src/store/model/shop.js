import request from '../../api'

export const shop = {
  state: {
    shop_id: '',
    shop_name: '',
    product_data: [],
  },

  reducers: {
    setShopState(state, payload) {
      return {
        ...state,
        shop_id: payload.shop_id,
        shop_name: payload.shop_name,
        product_data: payload.product_data,
      }
    },
  },
  effects: (dispatch) => ({
    async getShopInfo(payload, rootState) {
      const { data } = await request.get(`/api/products`)
      dispatch.shop.setShopState(data[0])
      // TODO: Should we use first array?
    },
  }),
}
