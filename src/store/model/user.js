import request from '../../api'
// import request from '../../utils/requestApi'
// import {get} from '../../utils/service'

export const user = {
  state: {
    line: {
      displayName: '',
      userId: '',
      pictureUrl: '',
      statusMessage: '',
    },

    chatty: {
      line_id: '',
      name: '',
      telephone: '',
      address: '',
      sub_district_id: 0,
    },

    chattyUserId: '',
    chattyUser: false,
    chattyShopId: '',
    chattyShop: false,
  },
  reducers: {
    // handle state changes with pure functions
    setUserState(state, payload) {
      console.log(`setUserState : ${JSON.stringify(payload)}`)
      return {
        ...state,
        line: {
          displayName: payload.displayName, // Source
          userId: payload.userId, // Source
          pictureUrl: payload.pictureUrl, // Source
          statusMessage: payload.statusMessage,
        },
        chatty: {
          ...state.chatty,
          line_id: payload.userId,
        },
      }
    },

    setChattyUserId(state, payload) {
      console.log(`setChattyUserId : ${payload}`)
      return {
        ...state,
        chattyUserId: payload,
      }
    },

    setUserStateByKey(state, payload) {
      console.log(`setUserState : ${JSON.stringify(payload)}`)
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    // async incrementAsync(payload, rootState) {
    //   await new Promise(resolve => setTimeout(resolve, 1000))
    //   dispatch.count.increment(payload)
    // }
    async checkUser(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      const res = await request.get(`/api/check_user?line_id=${payload}`)
      //   // const res = await request.get(`/check_user`, config)
      // const res = await get(`/check_user?line_id=${payload}`)

      if (res) {
        console.log(res.data)
        const { message } = res.data
        if (message.includes('Success')) {
          console.log(`checkUser Success :: chattyUserId is ${res.data.user_id} `)
          this.setChattyUserId(res.data.user_id)
          this.setUserStateByKey({ chattyUser: true })
          return true
        } else {
          console.log(`checkUser Fail :: ${message} `)
          // If Unsuccess check call create user immediately
          await this.creatUser()
          return true
        }
      }
    },

    async creatUser(payload, rootState) {
      console.log(`rootState : ${JSON.stringify(rootState.user)}`)
      // console.log(`creatUser : ${payload}`)
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      const res = await request.post(`/api/user`, rootState.user.chatty)
      if (res) {
        console.log(res.data)
        const { message } = res.data
        if (message.includes('Completed')) {
          console.log(`create User Success :: chattyUserId is ${res.data.id} `)
          this.setChattyUserId(res.data.id)
          this.setUserStateByKey({ chattyUser: true })
        } else {
          console.log(`createUser Fail :: ${message} `)

          // this set chattyUser true because it is end of checking userId process
          this.setUserStateByKey({ chattyUser: true })
        }
      }
    },

    async checkShop(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      const res = await request.get(`/api/check_shop/line/${payload.userId}`)

      if (res) {
        console.log(res.data)
        const { message } = res.data
        if (message.includes('already')) {
          console.log(`checkShop Success :: chattyShopId is ${res.data.shop_id} `)
          this.setUserStateByKey({ chattyShopId: res.data.shop_id })
          this.setUserStateByKey({ chattyShop: true })
          return true
        } else {
          console.log(`checkShop Fail :: ${message} `)
          // If Unsuccess check call create user immediately
          return true
        }
      }
    },
  }),
}
