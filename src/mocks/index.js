const url1 = 'https://static4.cilory.com/273124-large_default/nologo-navy-casual-shirt.jpg'
const url2 = 'https://de9luwq5d40h2.cloudfront.net/catalog/product/large_image/12_41435100022.jpg'

export const MOCK_PRODUCT = [
  {
    _id: '1',
    code: '1234',
    name: 'This is mock item',
    coverUrl: url1,
    additionData: 'nothing',
    weight: 5,
    prize: 900,
    quantity: 5,
    reg: 50,
    ems: 100,
    chargeByPostOffice: true,
    chargeByMyself: false,
    chargeByKerry: false,
    color: 'ดำ',
    size: 'S',
  },
]

export const MOCK_PRODUCTS = [
  {
    _id: '1',
    header: 'Charlotte set',
    prize: '890',
    cover: url1,
    type: 'polyester',
  },
  {
    _id: '2',
    header: 'Charlotte set 2',
    prize: '890',
    cover: url2,
    type: 'polyester',
  },
  {
    _id: '3',
    header: 'Charlotte set 3',
    prize: '890',
    cover: url1,
    type: 'polyester',
  },
  {
    _id: '4',
    header: 'Charlotte set 4',
    prize: '890',
    cover: url2,
    type: 'polyester',
  },
  {
    _id: '5',
    header: 'Charlotte set ',
    prize: '890',
    cover: url1,
    type: 'polyester',
  },
  {
    _id: '6',
    header: 'Charlotte set 6',
    prize: '890',
    cover: url2,
    type: 'polyester',
  },
  {
    _id: '7',
    header: 'Charlotte set 7',
    prize: '890',
    cover: url1,
    type: 'polyester',
  },
  {
    _id: '8',
    header: 'Charlotte set 8',
    prize: '890',
    cover: url1,
    type: 'polyester',
  },
]

export const MOCK_PRODUCTS2 = [
  {
    _id: '6',
    header: 'Charlotte set 6',
    prize: '890',
    cover: url1,
    type: 'polyester',
  },
  {
    _id: '7',
    header: 'Charlotte set 7',
    prize: '890',
    cover: url2,
    type: 'polyester',
  },
  {
    _id: '7',
    header: 'Charlotte set 7',
    prize: '890',
    cover: url2,
    type: 'polyester',
  },
  {
    _id: '8',
    header: 'Charlotte set 7',
    prize: '890',
    cover: url2,
    type: 'polyester',
  },
]

export const MOCK_ORDERS = [
  { _id: 1, type: 'packing' },
  { _id: 2, type: 'packing' },
  { _id: 3, type: 'delivery' },
  // { _id: 4, type: "notified" },
  { _id: 4, type: 'announce' },
  { _id: 5, type: 'delivery' },
  { _id: 6, type: 'packing' },
  // { _id: 7, type: "notified" },
  { _id: 7, type: 'announce' },
  { _id: 8, type: 'waitslip' },
]
export const MOCK_PRODUCT_GROUPS = [
  { _id: 1, groupNumber: '001', groupName: 'Charlotte set' },
  { _id: 2, groupNumber: '002', groupName: 'Charlotte set' },
]
