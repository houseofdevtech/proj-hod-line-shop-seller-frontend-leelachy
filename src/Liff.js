import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import Route from './Route'

const liff = window.liff

const Liff = (props) => {
  // useState mounted to Control useEffect as ComponentDidMount
  // const mounted = false
  // const { checkUser  } = props

  useEffect(() => {
    const initialize = () => {
      // console.log('initialize')
      liff.init(
        async (data) => {
          let profile = await liff.getProfile()
          console.log(`profile :: ${JSON.stringify(profile)}`)
          // props.setUserState(profile)
          console.log(`profile.userId :: ${profile.userId}`)
          console.log(`checkUser from liff init`)
          // props.checkUser(profile.userId)
          console.log('LIFF init OK')
        },
        (err) => {
          console.log('LIFF init failed')
        },
      )
    }
    // console.log(`.env :: ${process.env.REACT_APP_DEV}`)
    if (process.env.REACT_APP_DEV === 'local') {
      props.setUserState({ userId: 'testSeller3' })
    }
    // console.log('AddEvent Listener')
    window.addEventListener('load', initialize)
    props.setUserState({ userId: 'Ue4896c3cdbe38bba70885c0cedb96e23bbb' })
    props.checkShop({ userId: 'Ue4896c3cdbe38bba70885c0cedb96e23bbb' })
    props.getShopInfo()
    // below comment is disable warning
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return <Route />
}

const mapStateToProps = (state) => ({
  user: state.user,
  shop: state.state,
})
const mapDispatchToProps = (dispatch) => {
  return {
    setUserState: dispatch.user.setUserState,
    checkUser: dispatch.user.checkUser,
    checkShop: dispatch.user.checkShop,
    getShopInfo: dispatch.shop.getShopInfo,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Liff)
