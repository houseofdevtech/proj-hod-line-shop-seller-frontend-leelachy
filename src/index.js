import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import Liff from './Liff'
import store from './store'

const Root = () => (
  <Provider store={store}>
    <Liff />
  </Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'))
