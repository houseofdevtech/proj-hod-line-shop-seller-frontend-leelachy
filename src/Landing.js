import { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'

const Landing = (props) => {
  useEffect(() => {
    const queryControl = (page) => {
      page === 'setting'
        ? props.history.push('/setting')
        : page === 'warehouse'
        ? props.history.push('/warehouse')
        : page === 'broadcast'
        ? props.history.push('/broadcast')
        : page === 'orderlist'
        ? props.history.push('/orderList')
        : props.history.push('/notFound')
    }

    // get value from queryString
    const values = queryString.parse(props.location.search)
    // console.log(`values page : ${values.page}`);
    // console.log(`values page : ${JSON.stringify(values)}`);

    'page' in values ? queryControl(values.page) : props.history.push('/main')
  })

  return null
}

export default withRouter(Landing)
