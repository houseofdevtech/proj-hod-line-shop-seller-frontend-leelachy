import React from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import Star from '../img/star.png'

import './ProductBox.css'

const ProductBox = (props) => {
  const productColors = _.chain(props.product.product_items)
    .map((item) => item.color.color_value)
    .compact()
    .uniq()
    .value()
    .slice(0, 3)
  const renderColors = () => {
    return productColors.map((color) => (
      <div
        className="product-box__description__details__circle"
        style={{ backgroundColor: color }}
      />
    ))
  }

  return (
    <div className="product-box">
      <div
        className="product-box__image"
        style={{
          backgroundImage: `url(${
            _.isEmpty(props.product.product_items)
              ? ''
              : props.product.product_items[0].main_img_path
          })`,
        }}
      ></div>
      <div className="product-box__description">
        <div className="product-box__description__details">
          <div className="product-box__description__details__header">
            {props.product.product_name}
          </div>
          <div className="product-box__description__details__prize">
            ฿ {props.product.product_items[0] ? props.product.product_items[0].price : 0}
          </div>
          <div className="product-box__description__details__type">
            <div>Type: {props.product.type}</div>
            <div className="product-box__description__details__circles">{renderColors()}</div>
          </div>
          <div>Free size: </div>
        </div>
        {!props.disableEdit && (
          <div className="product-box__description__button">
            <img src={Star} alt="star" />
            <Link to={`/product?product_id=${props.product.product_id}`}>
              <Button>แก้ไขข้อมูลสินค้า</Button>
            </Link>
          </div>
        )}
      </div>
    </div>
  )
}

ProductBox.propTypes = {
  product: PropTypes.object.isRequired,
  shopId: PropTypes.number,
}

export default ProductBox
