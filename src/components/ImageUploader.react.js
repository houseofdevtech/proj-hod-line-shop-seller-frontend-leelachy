import React from 'react'
import _ from 'lodash'
import PropTypes from 'prop-types'

import ImagePreview from './ImagePreview.react'
import ImageUploadMenu from './ImageUploadMenu.react'

import './ImageUploader.css'

class ImageUploader extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      openUploadMenu: false,
      currentSelectedPosition: null,
    }
    this.fileInputRef = React.createRef()
  }

  getInitialState = () => {
    return { file: [] }
  }

  onChange = () => {
    const file = this.fileInputRef.current.files[0]

    const reader = new FileReader()
    const url = reader.readAsDataURL(file)

    this.onImageUploadSelect(file)
    this.onUploadMenuClose()
  }

  onUploadMenuOpen = (e) => {
    if (e) {
      this.setState({
        openUploadMenu: true,
        currentSelectedPosition: null,
      })
    } else {
      this.setState({ openUploadMenu: true })
    }
  }

  onUploadMenuClose = () => {
    this.setState({ openUploadMenu: false })
  }

  onImagePreviewClick = (position) => {
    this.setState({ currentSelectedPosition: position })
    this.onUploadMenuOpen()
  }

  onImageUpload = () => {
    this.fileInputRef.current.click()
  }

  onCoverImageSet = () => {
    this.onMainImageSet()
    this.onUploadMenuClose()
  }

  uploadProductImage = (url, file) => {
    const currentImagePosition = this.state.currentSelectedPosition
    const copyImages = this.props.images.slice(0)

    if (this.props.showOneItem) {
      copyImages[0].url = url
    } else {
      copyImages[currentImagePosition].url = url
      copyImages[currentImagePosition].file = file
    }

    return copyImages
  }

  onImageUploadSelect = (file) => {
    const currentImagePosition = this.state.currentSelectedPosition
    const isNoImages = _.isEmpty(this.props.images)
    let images
    let fileData
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => {
      fileData = e.target.result

      if (currentImagePosition === 0 || currentImagePosition > 0) {
        images = this.uploadProductImage(fileData, file)
      } else {
        images = [...this.props.images, { url: fileData, isCoverImage: isNoImages, file }]
      }
      console.log('image file', images)

      this.props.onImageUploadSelect(images)
      this.setState({ currentSelectedPosition: null })
    }
  }

  onMainImageSet = () => {
    const currentImagePosition = this.state.currentSelectedPosition
    const result = this.props.images.slice(0)
    const itemWantToChange = result[currentImagePosition]
    result[0].isCoverImage = false
    itemWantToChange.isCoverImage = true

    const primaryImage = { ...result[0] }
    const secondary = { ...itemWantToChange }

    result[0] = secondary
    result[currentImagePosition] = primaryImage

    this.props.onImageCoverSet(result)
  }

  renderPreviewImages = () => {
    if (_.isEmpty(this.props.images)) {
      return
    }

    if (this.props.showOneItem) {
      return (
        <ImagePreview
          item={this.props.images[0]}
          count={0}
          onPreviewImageClick={() => this.onImagePreviewClick(0)}
        />
      )
    }

    return this.props.images.map((item, index) => (
      <ImagePreview
        key={index}
        item={item}
        count={index}
        onPreviewImageClick={() => this.onImagePreviewClick(index)}
      />
    ))
  }

  renderAddContainer = () => {
    if (!_.isEmpty(this.props.images) && this.props.showOneItem) {
      return
    }

    return (
      <div className="image-uploader__button" onClick={this.onUploadMenuOpen}>
        <div className="image-uploader__add-container">
          <div className="image-uploader__add-button">
            <img src={require('../img/uploader.png')} alt="uploader" />
          </div>
          <div className="image-uploader__add-text">เพิ่มรูปภาพ</div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="image-uploader">
        {this.renderPreviewImages()}
        {this.renderAddContainer()}
        <input
          ref={this.fileInputRef}
          type="file"
          accept="image/*"
          name="user[image]"
          onChange={this.onChange}
        />
        {!this.props.disabled && (
          <ImageUploadMenu
            position={this.state.currentSelectedPosition}
            onCoverImageSet={this.onCoverImageSet}
            onUploadMenuClose={this.onUploadMenuClose}
            openUploadMenu={this.state.openUploadMenu}
            onImageUpload={this.onImageUpload}
          />
        )}
      </div>
    )
  }
}

ImageUploader.propTypes = {
  images: PropTypes.array.isRequired,
  onImageUploadSelect: PropTypes.func.isRequired,
  onImageCoverSet: PropTypes.func,
  showOneItem: PropTypes.bool,
  disabled: PropTypes.bool,
}

export default ImageUploader
