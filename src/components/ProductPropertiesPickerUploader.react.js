import React, { useState } from 'react'
import { SliderPicker, SketchPicker } from 'react-color'

import Upload from '../img/uploader.png'
import ImageUploadMenu from './ImageUploadMenu.react'

import './ProductPropertiesPickerUploader.css'

// TODO: Use React createPortal
const ProductPropertiesPickerUploader = (props) => {
  const [uploadMenuVisible, setUploadMenuVisible] = useState(false)
  const [sketchModalVisible, setSketchModalVisible] = useState(false)
  const [color, setColor] = useState('')
  const fileInputRef = React.createRef()

  const onImageUpload = () => {
    fileInputRef.current.click()
  }

  const onImageChange = () => {
    const file = fileInputRef.current.files[0]
    const reader = new FileReader()
    reader.readAsDataURL(file)
 
    reader.onloadend = () => {
      props.onPropertySelected(reader.result, file)
      setUploadMenuVisible(false)
    }
  }

  const handleChangeComplete = (selectedColor) => {
    setColor(selectedColor.hex)
    props.onPropertySelected(selectedColor.hex)
  }

  const renderSketchPicker = () => {
    if (!sketchModalVisible) return

    return (
      <div className="product-properties-picker-uploader__sketch-picker">
        <SketchPicker color={color} onChangeComplete={handleChangeComplete} />
      </div>
    )
  }

  return (
    <>
      <div className="product-properties-picker-uploader">
        <div
          className="product-properties-picker-uploader__add"
          onClick={() => setSketchModalVisible(!sketchModalVisible)}
        >
          +
        </div>
        {!sketchModalVisible && (
          <div
            className="product-properties-picker-uploader__image-upload"
            onClick={() => setUploadMenuVisible(true)}
          >
            <img src={Upload} alt="upload" />
          </div>
        )}
        <SliderPicker color={color} onChangeComplete={handleChangeComplete} />
      </div>
      {renderSketchPicker()}
      <input
        ref={fileInputRef}
        type="file"
        accept="image/*"
        name="user[image]"
        onChange={onImageChange}
        style={{ display: 'none' }}
      />
      <ImageUploadMenu
        openUploadMenu={uploadMenuVisible}
        onUploadMenuClose={() => setUploadMenuVisible(false)}
        onImageUpload={onImageUpload}
      />
    </>
  )
}

export default ProductPropertiesPickerUploader
