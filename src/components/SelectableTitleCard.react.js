import React from 'react'
import _ from 'lodash'
import PropTypes from 'prop-types'

import SelectableCard from './SelectableCard.react'

const SelectableTitleCard = (props) => {
  const onItemDelete = () => {
    props.onItemDelete(props.item.value)
  }

  return (
    <SelectableCard
      item={props.item}
      onClick={props.enableRemoveItem ? _.noop : props.onClick}
      enableRemoveItem={props.enableRemoveItem}
      selected={props.selected}
      count={props.count}
      countEnable={props.countEnable}
    >
      <div className="content">
        <span className="title">{props.item.value ? props.item.value : props.item}</span>
      </div>
      {props.enableRemoveItem && (
        <div className="close" onClick={onItemDelete}>
          x
        </div>
      )}
    </SelectableCard>
  )
}

SelectableTitleCard.propTypes = {
  item: PropTypes.object.isRequired,
  enableRemoveItem: PropTypes.bool,
  onItemDelete: PropTypes.func,
  onClick: PropTypes.func.isRequired,
}

export default SelectableTitleCard
