import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import './AddButtonWithText.css'

const AddButtonWithText = (props) => {
  return (
    <div className="add-button-with-text">
      <Link to={`/${props.url}`}>
        <div className="add-button-with-text__items">
          <div>+</div>
          <div>{props.text}</div>
        </div>
      </Link>
    </div>
  )
}

AddButtonWithText.propTypes = {
  url: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default AddButtonWithText
