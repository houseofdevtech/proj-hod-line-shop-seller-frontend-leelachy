import React from 'react'
import Timekeeper from 'react-timekeeper'

import CancelBtnIcon from '../img/ic-cancelbtn-gray-24-px.svg'
import CustomModal from './CustomModal'

import './ClockModal.css'

// const customModalStyle = {
//   overlay: {
//     position: 'fixed',
//     top: 0,
//     left: 0,
//     right: 0,
//     bottom: 0,
//     backgroundColor: 'rgba(38, 38, 38, 0.6)',
//     // zIndex: 20,
//   },
//   content: {
//     position: 'absolute',
//     top: '0',
//     left: '0',
//     right: '0',
//     bottom: '0',
//     border: '1px solid #ccc',
//     background: '#fff',
//     overflow: 'hidden',
//     WebkitOverflowScrolling: 'touch',
//     borderRadius: '10px',
//     outline: 'none',
//     padding: '0',
//     width: '400px',
//     // height: 'max-content',
//     height: '200px',
//     margin: 'auto',
//   },
// }

const mobileModalStyle = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(38, 38, 38, 0.6)',
    // zIndex: 20,
  },
  content: {
    position: 'absolute',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    border: '1px solid #ccc',
    background: '#fff',
    // overflow: 'scroll',
    overflow: 'hidden',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '10px',
    outline: 'none',
    padding: '0',
    width: 'max-content',
    height: 'max-content',
    margin: 'auto',
  },
}

const ClockModal = (props) => {
  // {console.log(`isOpen: ${props.isOpen}`)}
  return (
    <div>
      <CustomModal
        onRequestClose={() => props.closeModal()}
        shouldCloseOnOverlayClick={true}
        isOpen={props.isOpen}
        customStyle={mobileModalStyle}
      >
        <img
          src={CancelBtnIcon}
          alt="close-icon"
          className="close-btn"
          onClick={props.closeModal}
        />

        <Timekeeper
          time={props.time}
          switchToMinuteOnHourSelect={true}
          hour24Mode
          onChange={(data) => {
            //console.log(data.formatted24)
            //console.log(data.formatted24.length )
            const time = data.formatted24.length < 5 ? 0 + data.formatted24 : data.formatted24
            props.onChange(time)
          }}
        />
        <div className="clock-modal_bottom-bar" onClick={() => props.closeModal()}>
          <div className="clock-modal_submit-btn">OK</div>
        </div>
      </CustomModal>
    </div>
  )
}

export default ClockModal
