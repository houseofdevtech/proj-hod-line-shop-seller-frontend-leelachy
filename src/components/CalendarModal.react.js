import React, { useState } from 'react'
import DayPicker, { DateUtils } from 'react-day-picker'
import Helmet from 'react-helmet'
import moment from 'moment'

import CustomModal from './CustomModal'

import './CalendarModal.css'
import 'react-day-picker/lib/style.css'

// import CancelBtnIcon from '../img/ic-cancelbtn-gray-24-px.svg'

// const customModalStyle = {
//   overlay: {
//     position: 'fixed',
//     top: 0,
//     left: 0,
//     right: 0,
//     bottom: 0,
//     backgroundColor: 'rgba(38, 38, 38, 0.6)',
//     // zIndex: 20,
//   },
//   content: {
//     position: 'absolute',
//     top: '0',
//     left: '0',
//     right: '0',
//     bottom: '0',
//     border: '1px solid #ccc',
//     background: '#fff',
//     overflow: 'hidden',
//     WebkitOverflowScrolling: 'touch',
//     borderRadius: '10px',
//     outline: 'none',
//     padding: '0',
//     width: '400px',
//     // height: 'max-content',
//     height: '200px',
//     margin: 'auto',
//   },
// }

const mobileModalStyle = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(38, 38, 38, 0.6)',
    // zIndex: 20,
  },
  content: {
    position: 'absolute',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    border: '1px solid #ccc',
    background: '#fff',
    // overflow: 'scroll',
    overflow: 'hidden',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '10px',
    outline: 'none',
    padding: '0',
    width: 'max-content',
    height: 'max-content',
    margin: 'auto',
  },
}

const CalendarModal = (props) => {
  const [state, setState] = useState({ from: undefined, to: undefined })

  const { from, to } = state
  const modifiers = { start: from, end: to }

  const handleDayClick = (day) => {
    const range = DateUtils.addDayToRange(day, state)
    props.setStartDate(moment(range.from).format('YYYY-MM-DD'))
    props.setEndDate(moment(range.to).format('YYYY-MM-DD'))
    setState(range)
  }
  const handleResetClick = () => {
    props.setStartDate('')
    props.setEndDate('')
    setState({ from: undefined, to: undefined })
  }
  // {console.log(`isOpen: ${props.isOpen}`)}

  return (
    <div>
      <CustomModal
        onRequestClose={() => props.closeModal()}
        shouldCloseOnOverlayClick={true}
        isOpen={props.isOpen}
        customStyle={mobileModalStyle}
      >
        <div className="calendar-modal_container">
          <DayPicker
            className="Selectable"
            numberOfMonths={1}
            selectedDays={[from, { from, to }]}
            modifiers={modifiers}
            onDayClick={handleDayClick}
          />
          <Helmet>
            <style>{`
  .Selectable .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
    background-color: #ffffff !important;
    color: #333333;
  }
  .Selectable .DayPicker-Day {
    border-radius: 0 !important;
  }
  .Selectable .DayPicker-Day--start {
    background-color: #333333 !important;
    border-top-left-radius: 40% !important;
    border-bottom-left-radius: 40% !important;
  }
  .Selectable .DayPicker-Day--end {
    background-color: #333333 !important;
    border-top-right-radius: 40% !important;
    border-bottom-right-radius: 40% !important;
  }
`}</style>
          </Helmet>
          <div className="calendar-modal_bottom-bar">
            <div className="calendar-modal_reset-btn" onClick={() => handleResetClick()}>
              Reset
            </div>
            <div className="calendar-modal_submit-btn" onClick={() => props.closeModal()}>
              OK
            </div>
          </div>
        </div>
      </CustomModal>
    </div>
  )
}

export default CalendarModal
