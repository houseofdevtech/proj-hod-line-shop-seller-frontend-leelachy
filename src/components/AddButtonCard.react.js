import React from 'react'
import { Link } from 'react-router-dom'
import { Card, Image } from 'semantic-ui-react'

import './AddButtonCard.css'

const AddButtonCard = (props) => {
  return (
    <div className="add-button-card">
      <Link to={props.path}>
        <div className="add-button-card__icon">+</div>
        <Card className="add-button-card__card">
          <Image src={props.photo} wrapped ui={false} />
          <div className="add-button-card__content">
            <span className="add-button-card__text">{props.title}</span>
          </div>
        </Card>
      </Link>
    </div>
  )
}
export default AddButtonCard
