import React, { useState } from 'react'
import ContentEditable from 'react-contenteditable'
import PropTypes from 'prop-types'

import './DraftProductProperties.css'

const DraftProductProperties = (props) => {
  const [draftTitle, setDraftTitle] = useState('ชื่อตัวเลือกสินค้า')
  const [draftSubItem, setDraftSubItem] = useState([])

  const onAddDraftSubItem = () => {
    setDraftSubItem([...draftSubItem, { [draftSubItem.length]: 'ลักษณะ' }])
  }

  const onSubItemValueChange = (position) => (e) => {
    const currentlyItem = { [position]: e.target.value }
    let result = draftSubItem
    const previousItem = result.find((item) => Object.keys(item)[0] === String(position))
    const mergedObject = { ...previousItem, ...currentlyItem }
    const indexOfDuplicatedItem = result.findIndex(
      (item) => Object.keys(item)[0] === String(position),
    )
    result[indexOfDuplicatedItem] = mergedObject
    setDraftSubItem(result)
  }

  const onDraftTitleChange = (e) => {
    setDraftTitle(e.target.value)
  }

  const onDraftTitleReset = () => {
    setDraftTitle('')
  }

  const onDraftItemDelete = () => {
    props.onDelete(props.identity)
  }

  const onResetContent = (index) => {
    const copyDraftSubItem = draftSubItem.slice(0)
    copyDraftSubItem[index][index] = null
    setDraftSubItem(copyDraftSubItem)
  }

  const renderDraftSubItem = () =>
    draftSubItem.map((item, index) => (
      <ContentEditable
        key={index}
        html={item[index]}
        onChange={onSubItemValueChange(index)}
        onClick={() => onResetContent(index)}
      />
    ))

  const onDraftPropertiesCreate = () => {
    props.onSave({ draftTitle, draftSubItem })
  }

  return (
    <div className="draft-product-properties">
      <div className="draft-product-properties__actions">
        <div className="draft-product-properties__actions__name">
          <div onClick={onDraftItemDelete}>-</div>
          <ContentEditable
            html={draftTitle}
            onChange={onDraftTitleChange}
            onClick={onDraftTitleReset}
          />
        </div>
        <div onClick={onDraftPropertiesCreate}>บันทึก</div>
      </div>
      <div className="draft-product-properties__items">
        {renderDraftSubItem()}
        <div onClick={onAddDraftSubItem}>​+ เพิ่ม</div>
      </div>
    </div>
  )
}

DraftProductProperties.propTypes = {
  identity: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
}

export default DraftProductProperties
