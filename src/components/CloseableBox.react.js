import React from 'react'

import './CloseableBox.css'

const CloseableBox = (props) => {
  return (
    <div className="closeable-box">
      <div className="closeable-box__name">{props.name}</div>
      {props.enableRemoveItem && (
        <span className="closeable-box__close-icon" onClick={props.onRemove}>
          x
        </span>
      )}
    </div>
  )
}

export default CloseableBox
