import React from 'react'

import CancelBtnIcon from '../img/ic-cancelbtn-gray-24-px.svg'
import CustomModal from './CustomModal'

import './ModalUpdateOrder.css'

// const customModalStyle = {
//   overlay: {
//     position: 'fixed',
//     top: 0,
//     left: 0,
//     right: 0,
//     bottom: 0,
//     backgroundColor: 'rgba(38, 38, 38, 0.6)',
//     // zIndex: 20,
//   },
//   content: {
//     position: 'absolute',
//     top: '0',
//     left: '0',
//     right: '0',
//     bottom: '0',
//     border: '1px solid #ccc',
//     background: '#fff',
//     overflow: 'hidden',
//     WebkitOverflowScrolling: 'touch',
//     borderRadius: '10px',
//     outline: 'none',
//     padding: '0',
//     width: '400px',
//     // height: 'max-content',
//     height: '200px',
//     margin: 'auto',
//   },
// }

const mobileModalStyle = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(38, 38, 38, 0.6)',
    // zIndex: 20,
  },
  content: {
    position: 'absolute',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    border: '1px solid #ccc',
    background: '#fff',
    // overflow: 'scroll',
    overflow: 'hidden',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '10px',
    outline: 'none',
    padding: '0',
    width: '250px',
    height: '175px',
    margin: 'auto',
  },
}

const ModalUpdateOrder = (props) => {
  // {console.log(`isOpen: ${props.isOpen}`)}
  return (
    <div>
      <CustomModal
        onRequestClose={() => props.closeModal()}
        shouldCloseOnOverlayClick={true}
        isOpen={props.isOpen}
        customStyle={mobileModalStyle}
      >
        <img
          src={CancelBtnIcon}
          alt="close-icon"
          className="close-btn"
          onClick={props.closeModal}
        />
        <div className="update-order__modal__content">
          <div>
            <div>กรุณากรอกรหัสผ่าน</div>
            <input
              type="tel"
              placeholder="รห้สผ่าน"
              pattern="[0-9]*"
              inputMode="numeric"
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
            />
            {props.isWrongPassword && <p> รหัสผ่านไม่ถูกต้อง </p>}
          </div>
          <div className="update-order__modal__action" onClick={() => props.onSubmit()}>
            <div className="update-order__modal__action_button">OK</div>
          </div>
        </div>
      </CustomModal>
    </div>
  )
}

export default ModalUpdateOrder
