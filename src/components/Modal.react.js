import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import OutsideDetection from './OutsideDetection.react.js'

import './Modal.css'

const Modal = (props) => {
  return (
    <div className="modal">
      <div className="modal__overlay">
        <OutsideDetection onClickOutSide={props.onCancel}>
          <div className="modal__content">
            <div className="modal__description">{props.description}</div>
            <div className="modal__actions">
              <Button onClick={props.onCancel}>{props.leftActionText}</Button>
              <Button onClick={props.onConfirm}>{props.rightActionText}</Button>
            </div>
          </div>
        </OutsideDetection>
      </div>
    </div>
  )
}

Modal.propTypes = {
  description: PropTypes.string.isRequired,
  leftActionText: PropTypes.string.isRequired,
  rightActionText: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
}

export default Modal
