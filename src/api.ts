import axios, { AxiosInstance } from 'axios'

let instance: AxiosInstance

switch (process.env.REACT_APP_DEV) {
  case 'leelachy':
    instance = axios.create({
      baseURL: 'https://chattyshop.houseofdev.tech/backend2',
    })

    break
  case 'dev':
    instance = axios.create({
      baseURL: 'https://chattyshop.houseofdev.tech/backend',
    })
    break
  case 'uat':
    instance = axios.create({
      baseURL: 'https://uat.chatty.shop/backend',
    })
    break
  default:
    // NOTED: local
    instance = axios.create({
      // baseURL: 'https://vulcan.houseofdev.tech/hod-line-shop/backend/api',
      // baseURL: 'https://uat.chatty.shop/backend/api',
      baseURL: 'https://chattyshop.houseofdev.tech/backend2',
    })
}

export default instance
