import React from 'react'
import { connect } from 'react-redux'
// import ReactDOM from "react-dom";
import {
  Route,
  Switch,
  // HashRouter
  BrowserRouter,
} from 'react-router-dom'

import App from './App.js'
import Loading from './components/Loading.react'
import BroadcastLoading from './containers/BroadcastLoading.react'
import CatalogBroadcast from './containers/CatalogBroadcast.react'
import ErrorBoundary from './containers/ErrorBoundary.react'
import FilterOrders from './containers/FilterOrders.react'
import ImageMapBroadcast from './containers/ImageMapBroadcast.react'
import NotFound from './containers/NotFound.react'
import OrderList from './containers/OrderList.react'
import Product from './containers/Product.react'
import ProductGroup from './containers/ProductGroup.react'
import ProductGroupList from './containers/ProductGroupList.react'
// import Recommends from "./containers/Recommends.react";
import SelectBroadcast from './containers/SelectBroadcast.react'
import Setting from './containers/Setting.react'
import UpdateOrder from './containers/UpdateOrder.react'
import Warehouse from './containers/Warehouse.react'
import Landing from './Landing.js'

const Routing = (props) => {
  if (!props.shop.shop_name) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}
      >
        <Loading />
      </div>
    )
  }
  return (
    <BrowserRouter basename="/leelachy/seller">
      <ErrorBoundary>
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route path="/main" component={App} />

          <Route path="/warehouse" component={Warehouse} />
          <Route path="/product" component={Product} />
          <Route path="/addProduct" component={Product} />

          {/* <Route path="/recommend" component={Recommends} /> */}
          <Route path="/broadcast" component={SelectBroadcast} />
          <Route path="/broadcasting" component={BroadcastLoading} />
          <Route path="/catalog" component={CatalogBroadcast} />
          <Route path="/imageMap" component={ImageMapBroadcast} />
          <Route path="/productGroupList" component={ProductGroupList} />
          <Route path="/productGroup" component={ProductGroup} />
          <Route path="/productGroup/:id" component={ProductGroup} />

          <Route path="/orderList" component={OrderList} />
          <Route path="/filterOrders" component={FilterOrders} />
          <Route path="/updateOrder/:id" component={UpdateOrder} />

          <Route path="/setting" component={Setting} />

          <Route component={NotFound} />
        </Switch>
      </ErrorBoundary>
    </BrowserRouter>
  )
}

const mapStateToProps = (state) => ({
  shop: state.shop,
})

export default connect(mapStateToProps)(Routing)
