import { ProductItem } from '../Product'

export type ProductGroupItem = {
  product_id: number
  product_code: string
  product_name: string
  product_items: ProductItem[]
  detail: string
  weight: number
  shipping_register: number
  shipping_ems: number
  status: number
}
