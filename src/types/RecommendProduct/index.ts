import { ProductItem } from '../Product'

export type RecommendItem = {
  rank_no: number
  product_id: number
  product_code: string
  product_name: string
  product_items: ProductItem[]
}

export type RecommendProduct = {
  shop_id: number
  recommended_id: number
  recommended_items: RecommendItem[]
}
