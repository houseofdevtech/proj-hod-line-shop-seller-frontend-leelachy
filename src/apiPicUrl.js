let picBaseURL = null

if (process.env.REACT_APP_DEV === 'local') {
  // picBaseURL = 'https://uat.chatty.shop/backend/'
  picBaseURL = 'https://chattyshop.houseofdev.tech/backend2'
}
if (process.env.REACT_APP_DEV === 'dev') {
  picBaseURL = 'https://chattyshop.houseofdev.tech/backend2/'
}
if (process.env.REACT_APP_DEV === 'uat') {
  picBaseURL = 'https://uat.chatty.shop/backend/'
}
if (process.env.REACT_APP_DEV === 'passione') {
  picBaseURL = 'https://chattyshop.houseofdev.tech/backend2'
}
if (process.env.REACT_APP_DEV === 'leelachy') {
  picBaseURL = 'https://chattyshop.houseofdev.tech/backend2'
}

export default picBaseURL
