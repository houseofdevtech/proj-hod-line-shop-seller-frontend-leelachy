import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import './App.css'

class App extends React.PureComponent {
  render() {
    return (
      <div>
        <Link to="/warehouse">Warehouse</Link>
        <br />
        <Link to="/broadcast">Broadcast</Link>
        <br />
        <Link to="/setting">Setting</Link>
        <br />
        <Link to="/orderlist">Orderlist</Link>
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  user: state.user,
})
const mapDispatchToProps = (dispatch) => {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(App)
