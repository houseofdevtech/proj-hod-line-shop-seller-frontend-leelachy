import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import _ from 'lodash'

import FloatingButtonAction from '../components/FloatingButtonAction.react'
import Loading from '../components/Loading.react'
import ProductBox from '../components/ProductBox.react'
import StickyBox from '../components/StickyBox.react'
import { getProducts } from '../rest'

import './Warehouse.css'

const Warehouse = (props) => {
  const [products, setProducts] = useState(null)
  const [loading, setLoading] = useState(true)
  const user = useSelector((state) => state.user)

  useEffect(() => {
    const fetchCurrentShopProducts = async () => {
      try {
        const response = await getProducts(user.chattyShopId, '')
        setLoading(false)
        setProducts(response.product_data)
      } catch (error) {
        console.log('Error:fetchCurrentShopProducts: ', error)
      }
    }
    fetchCurrentShopProducts()
  }, [user.chattyShopId])

  const renderProducts = () => {
    if (_.isEmpty(products)) {
      return (
        <div className="warehouse__products__empty">
          <img src={require('../img/packing.png')} alt="packing" />
          <div className="warehouse__products__empty__description">
            คุณยังไม่มีสินค้าในคลังสินค้า กรุณากดปุ่มด้านล่างเพื่อเพิ่มสินค้า
          </div>
          <div
            className="warehouse__products__empty__add-product"
            onClick={() => props.history.push('addProduct')}
          >
            <span>+</span>
          </div>
        </div>
      )
    }
    return products.map((item, index) => (
      <ProductBox key={index} product={item} shopId={user.chattyShopId} />
    ))
  }

  if (loading) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}
      >
        <Loading />
      </div>
    )
  }

  return (
    <div className="warehouse">
      <StickyBox title="คลังสินค้า" />
      <div className="warehouse__products">{renderProducts()}</div>
      {!_.isEmpty(products) && <FloatingButtonAction />}
    </div>
  )
}

export default Warehouse
