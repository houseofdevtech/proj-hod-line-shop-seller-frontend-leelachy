import React, { useState, useEffect } from 'react'
import moment from 'moment'
import { Input } from 'semantic-ui-react'

import request from '../api'
// import CancelBtnIcon from '../img/ic-cancelbtn-gray-24-px.svg'
import ModalUpdateOrder from '../components/ModalUpdateOrder.react'
import BackArrow from '../img/back_arrow.png'

import './UpdateOrder.css'

const UpdateOrder = (props) => {
  const [orderCode, setOrderCode] = useState('')
  const [refBillCode, setRefBillCode] = useState('')
  const [comment, setComment] = useState('')
  const [status, setStatus] = useState(props.location.state.orderStatus)
  const [message, setMessage] = useState({})
  const [showModal, setShowModal] = useState(false)
  const [holdStatus, setHoldStatus] = useState('')
  const [password, setPassword] = useState('')
  const [isWrongPassword, setIsWrongPassword] = useState(false)
  const item = props.location.state.itemData

  //disable warning
  comment !== '' && console.log(comment)

  const editTracking = async (obj) => {
    request.interceptors.request.use((request) => {
      console.log('Starting Request', request)
      return request
    })
    request.interceptors.response.use((response) => {
      console.log('Response:', response)
      return response
    })
    const body = {
      pos: obj.refBillCode,
      tracking_number: obj.trackingNumber,
      status: obj.status,
    }
    console.log(body)
    try {
      const { data } = await request.post(`/api/tracking/${obj.trackingId}`, body)
      if (data.message) {
        console.log(`editTracking response : ${JSON.stringify(data)}`)
        props.history.goBack()
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    const { tracking_number } = item
    if (tracking_number !== null) {
      setOrderCode(tracking_number)
    }
    // intend to useEffect as ComponentDidMount()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    const { pos } = item
    if (pos !== null) {
      setRefBillCode(pos)
    }
    // intend to useEffect as ComponentDidMount()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    const prepareMessage = () => {
      return {
        trackingId: props.location.state.trackingId,
        status:
          status === 'waitslip'
            ? 0
            : status === 'packing'
            ? 1
            : status === 'delivery'
            ? 2
            : status === 'announce'
            ? 3
            : status === 'cancel'
            ? 4
            : null,
        trackingNumber: orderCode,
        refBillCode: refBillCode,
      }
    }
    const draftMessage = prepareMessage()
    // console.log(JSON.stringify(draftMessage))
    console.log(draftMessage)
    setMessage(draftMessage)
  }, [orderCode, props.location, status, refBillCode])

  useEffect(() => {}, [password])

  const onStatusChange = (e) => {
    console.log(e.target.value)

    if (status === 'waitslip' && e.target.value !== 'waitslip') {
      setHoldStatus(e.target.value)
      // show Modal
      setShowModal(true)
    } else {
      setStatus(e.target.value)
    }
  }

  const toggleModal = () => setShowModal(!showModal)

  const handleSetPassword = (value) => {
    if (isWrongPassword) {
      //if error msg show remove error msg while next onChange
      setIsWrongPassword(false)
      setPassword(value)
    } else {
      // set normally password
      setPassword(value)
    }
  }

  const handleCloseModal = () => {
    setPassword('') // clear password
    setIsWrongPassword(false) // clear error message
    toggleModal() // close modal
  }

  const submitPassword = () => {
    if (password === '1122') {
      setStatus(holdStatus)
      setPassword('')
      toggleModal()
    } else {
      console.log(password)
      setIsWrongPassword(true)
    }
  }

  const { date_order } = item
  const { name, address, province, district, sub_district } = item.tracking_items.data_user

  const formattedOrderDate = moment(date_order).format('DD/MM/YY : HH.MM น.')
  const formattedAddress = `${address} ${sub_district.name_in_thai} ${district.name_in_thai} จังหวัด${province.name_in_thai} `
  // console.log(`status: ${status}`)
  return (
    <div className="update-order">
      <div className="update-order__detail">
        {/* <div>ชื่อ นางสาวสวย ช้อปปิ้งเก่ง</div>
        <div>วันที่สั่งสินค้า : 12-02-2019</div>
        <div>ที่อยู่จัดส่ง: </div>
        <div>123/456 คอนโดทวีสุข ตึก เอ แขวงบางซื่อ เขตบางซื่อ กรุงเทพฯ 10800</div> */}
        <div>ชื่อ คุณ{name}</div>
        <div>วันที่สั่งสินค้า : {formattedOrderDate}</div>
        <div>ที่อยู่จัดส่ง: </div>
        <div>{formattedAddress}</div>
        <div className="update-order__detail__divider" />
        <div className="update-order__detail__input-data">
          <div>
            <input
              type="radio"
              name="status"
              value="waitslip"
              checked={status === 'waitslip'}
              onChange={onStatusChange}
            />
            <span>รอยืนยันการชำระเงิน</span>
          </div>
          <div>
            <input
              type="radio"
              name="status"
              value="packing"
              checked={status === 'packing'}
              onChange={onStatusChange}
            />
            <span>รอบรรจุ</span>
          </div>
          <div>
            <input
              type="radio"
              name="status"
              value="delivery"
              checked={status === 'delivery'}
              onChange={onStatusChange}
            />
            <span>รอจัดส่ง</span>
          </div>
          <div>
            <input
              type="radio"
              name="status"
              value="announce"
              checked={status === 'announce'}
              onChange={onStatusChange}
            />
            <span>แจ้งเลขพัสดุ</span>
            <br />
            <Input
              type="text"
              size="small"
              placeholder="กรุณากรอกเลขพัสดุ..."
              value={orderCode}
              //onChange={(e) => setOrderCode(e.target.value)}
              onChange={(e) => setOrderCode(e.target.value)}
            />
            <br />
            <span>รหัสบิลอ้างอิง</span>
            <br />
            <Input
              type="text"
              size="small"
              placeholder="กรุณากรอกรหัสบิล..."
              value={refBillCode}
              //onChange={(e) => setOrderCode(e.target.value)}
              onChange={(e) => setRefBillCode(e.target.value)}
            />
          </div>
          <div>
            <input
              type="radio"
              name="status"
              value="cancel"
              checked={status === 'cancel'}
              onChange={onStatusChange}
            />
            <span>ยกเลิกรายการ</span>
            <br />
            <Input
              type="text"
              size="small"
              placeholder="กรุณากรอกเหตุผลในการยกเลิกรายการ..."
              onChange={(e) => setComment(e.target.value)}
            />
          </div>
        </div>
        <div className="update-order__detail__actions">
          <div className="update-order__detail__back-button" onClick={() => props.history.goBack()}>
            <img src={BackArrow} alt="back-button" />
          </div>
          <div className="update-order__detail__button" onClick={() => editTracking(message)}>
            บันทึก
          </div>
        </div>
      </div>

      <ModalUpdateOrder
        isOpen={showModal}
        isWrongPassword={isWrongPassword}
        value={password}
        onChange={handleSetPassword}
        onSubmit={submitPassword}
        closeModal={() => {
          handleCloseModal()
        }}
      />
    </div>
  )
}

export default UpdateOrder
