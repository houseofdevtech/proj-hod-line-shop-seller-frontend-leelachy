import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import _ from 'lodash'
import { Button } from 'semantic-ui-react'

import BottomBar from '../components/BottomBar.react'
import ImageUploader from '../components/ImageUploader.react'
import ProductBox from '../components/ProductBox.react'
import StickyBox from '../components/StickyBox.react'
import noneSelectProduct from '../img/noneSelectProduct.png'
import { createBroadcast, getProductGroupShop } from '../rest'

import './ImageMapBroadcast.css'

const ImageMapBroadcast = (props) => {
  const [mode, setMode] = useState('multiple')
  const [productGroups, setProductGroups] = useState([])
  const [productImages, setProductImages] = useState([])
  const user = useSelector((state) => state.user)

  useEffect(() => {
    const fetchProductGroups = async () => {
      const response = await getProductGroupShop(user.chattyShopId)
      const groupMainImages = _.map(response.product_group_data, (group) => [
        { url: group.main_img_path, isCoverImage: true },
      ])

      setProductGroups(response.product_group_data)
      setProductImages(groupMainImages)
    }
    fetchProductGroups()
  }, [user.chattyShopId])

  const onSetImageMapMode = (e) => {
    setMode(e.target.getAttribute('data-mode'))
  }

  // TODO: What is broadcast_code and how to get an items properties in payload
  const onSubmit = async () => {
    try {
      await createBroadcast(user.chattyShopId, {
        body_data: {
          broadcast_code: 'xxxxx', // mock
          type: 'image_map',
          items: [1, 2, 3], // mock
          path_gcs_name: '', // mock
        },
      })

      props.history.goBack()
    } catch (error) {
      console.error('Error:createBroadcast: ', error)
    }
  }

  const onImageUploadSelect = (position) => (images) => {
    const currentImages = productImages.slice(0)
    currentImages[position] = [{ url: images[0].url, isCoverImage: true }]
    setProductImages(currentImages)
  }

  const renderProductGroups = () => {
    if (_.isEmpty(productGroups)) {
      return null
    }

    return productGroups.map((item, index) => (
      <div className="image-map-broadcast__product-groups" key={index}>
        <div className="image-map-broadcast__product-groups__title">ข้อมูลรูปที่ {index + 1}</div>
        <div className="image-map-broadcast__product-groups__top">
          <ImageUploader
            images={productImages[index]}
            onImageUploadSelect={onImageUploadSelect(index)}
            showOneItem
          />
          <div className="image-map-broadcast__product-groups__top__button">
            <Link
              to={{
                pathname: '/productGroupList',
                state: {
                  groupId: item.product_group_id,
                },
              }}
            >
              <Button>แก้ไขกลุ่มสินค้า</Button>
            </Link>
          </div>
        </div>
        <div className="image-map-broadcast__product-groups__divider" />
        <div className="image-map-broadcast__product-groups__bottom">
          <div className="image-map-broadcast__product-groups__bottom__title">
            กลุ่มสินค้าของรูปที่ {index + 1} : <b>{item.name}</b>
          </div>
          <div className="image-map-broadcast__product-groups__bottom__product-groups">
            {item.product_group_items.map((product, index) => (
              <ProductBox key={index} product={product} disableEdit />
            ))}
          </div>
        </div>
      </div>
    ))
  }

  const renderNonSelectedProductGroup = () => {
    if (mode === 'single') {
      return null
    }

    return (
      <div>
        <div className="image-map-broadcast__none-select-product-groups">
          <div className="image-map-broadcast__none-select-product-groups__title">
            ข้อมูลรูปที่ 2
          </div>
          <div className="image-map-broadcast__none-select-product-groups__bottom">
            <div className="image-map-broadcast__none-select-product-groups__bottom__image-container">
              <div className="image-map-broadcast__none-select-product-groups__bottom__image-container__top">
                <img src={noneSelectProduct} alt="noneSelectProduct" />
              </div>
              <div className="image-map-broadcast__none-select-product-groups__bottom__image-container__bottom">
                ภาพปก
              </div>
            </div>
            <Link to="/productGroupList">
              <div className="image-map-broadcast__none-select-product-groups__button">
                <Button>เพิ่มสินค้าจากกลุ่ม</Button>
              </div>
            </Link>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="image-map-broadcast">
      <StickyBox title="การตั้งค่า" backButtonDisable />
      <div className="image-map-broadcast__options">
        <div
          className={classNames('image-map-broadcast__options__item', {
            '--selected': mode === 'single',
          })}
          data-mode="single"
          onClick={onSetImageMapMode}
        >
          <div>Image Map แบบ 1 รูป</div>
          <div className="image-map-broadcast__options__item__single">1</div>
        </div>
        <div
          className={classNames('image-map-broadcast__options__item', {
            '--selected': mode === 'multiple',
          })}
          data-mode="multiple"
          onClick={onSetImageMapMode}
        >
          <div>Image Map แบบ 4 รูป</div>
          <div className="image-map-broadcast__options__item__multiple">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
          </div>
        </div>
      </div>
      {renderProductGroups()}

      {renderNonSelectedProductGroup()}
      <BottomBar
        buttonText="บันทึกการตั้งค่า"
        onBack={() => props.history.goBack()}
        onSubmit={onSubmit} // TODO: Send data to request
      />
    </div>
  )
}

export default ImageMapBroadcast
