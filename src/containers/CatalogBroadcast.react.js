import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import BottomBar from '../components/BottomBar.react'
import ProductBox from '../components/ProductBox.react'
import SelectableCardList from '../components/SelectableCardList.react'
import StickyBox from '../components/StickyBox.react'
import { createRecommendedProduct, getRecommendedProducts } from '../rest'

import './CatalogBroadcast.css'

const CatalogBroadcast = (props) => {
  const [selectedItems, setSelectedItems] = useState([])
  const [recommendProducts, setRecommendProducts] = useState([])

  useEffect(() => {
    const fetchRecommendProducts = async () => {
      const response = await getRecommendedProducts()
      setRecommendProducts(response[0].recommended_items)
    }
    fetchRecommendProducts()
  }, [])

  const onGetSelectedItems = (items) => {
    setSelectedItems(items.map((item) => item.props.product.product_id))
  }

  const onCreateRecommendProduct = async () => {
    try {
      await createRecommendedProduct({
        shop_id: props.user.chattyShopId,
        product_id: selectedItems,
      })
      props.history.goBack()
    } catch (error) {
      console.log('ERROR [onCreateRecommendProduct]: ', error)
    }
  }

  const renderProductBoxes = () => {
    if (!recommendProducts) return

    return recommendProducts.map((product, index) => (
      <ProductBox product={product} count={index + 1} shopId={props.user.chattyShopId} />
    ))
  }

  return (
    <div className="catalog-broadcast">
      <StickyBox title="Broadcast" />
      <div className="catalog-broadcast__description">กรุณาเลือกสินค้าเพื่อทำการบรอดแคสต์</div>

      <div className="catalog-broadcast__products">
        <Link to="/productGroup">
          <div className="catalog-broadcast__add-button-container">
            <div>+</div>
            <div>เพิ่มกลุ่มสินค้า</div>
          </div>
        </Link>
        <div className="catalog-broadcast__add-button-container__lists">
          <SelectableCardList
            contents={renderProductBoxes()}
            maxSelectable={10}
            onGetSelectedItems={onGetSelectedItems}
            multiple
            countEnable
          />
        </div>
      </div>
      {!_.isEmpty(recommendProducts) && (
        <BottomBar
          buttonText="ยืนยันการเลือกสินค้า"
          buttonDisable={_.isEmpty(selectedItems)}
          onBack={() => props.history.goBack()}
          onSubmit={onCreateRecommendProduct}
        />
      )}
    </div>
  )
}

const mapStateToProps = (state) => ({
  user: state.user,
})

export default connect(mapStateToProps)(CatalogBroadcast)
