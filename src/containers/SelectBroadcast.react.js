import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { Button } from 'semantic-ui-react'

import StickyBox from '../components/StickyBox.react'
import { getBroadcastShop } from '../rest'

import './SelectBroadcast.css'

const SelectBroadcast = () => {
  const user = useSelector((state) => state.user)
  const [broadcasts, setBroadcasts] = useState([])
  useEffect(() => {
    const fetchBroadcast = async () => {
      const response = await getBroadcastShop(user.chattyShopId)
      setBroadcasts(response.broadcast_data)
    }

    fetchBroadcast()
  }, [user.chattyShopId])

  const renderBroadcastHistory = () => {
    if (_.isEmpty(broadcasts)) {
      return null
    }

    return broadcasts.map((item) => (
      <div className="select-broadcast__histories__each-container">
        <div className="select-broadcast__histories__each-container__top">
          <div className="select-broadcast__histories__each-container__top__left">
            <div>Broadcast ID :</div>
            <div>รูปแบบในการบรอดแคสต์ :</div>
          </div>

          <div className="select-broadcast__histories__each-container__top__right">
            <div>{item.broadcast_id}</div>
            <div>{item.broadcast_type}</div>
          </div>
        </div>
        {/* <div className="select-broadcast__histories__each-container__bottom">
        <Button>ดูตัวอย่างและบรอดแคสต์ซ้ำ</Button>
    </div>  */}
      </div>
    ))
  }

  return (
    <div className="select-broadcast">
      <StickyBox title="จัดการ Broadcast" />
      <div className="select-broadcast__broadcast-selection">
        <div className="select-broadcast__broadcast-selection__title">
          <div>สร้างการ ​Broadcast ทันที</div>
          <div>กรุณาเลือกรูปแบบในการบรอดแคสต์</div>
        </div>
        <div className="select-broadcast__broadcast-selection__options">
          <Link to="/catalog">
            <div className="select-broadcast__broadcast-selection__options__catalog">
              <div className="select-broadcast__broadcast-selection__options__catalog__button">
                <div>+</div>
                <div>Catalog</div>
              </div>
            </div>
          </Link>
          <Link to="/imageMap">
            <div className="select-broadcast__broadcast-selection__options__image-map">
              <div className="select-broadcast__broadcast-selection__options__catalog__button">
                <div>+</div>
                <div>Image Map</div>
              </div>
            </div>
          </Link>
        </div>
      </div>

      <div className="select-broadcast__histories">
        <div className="select-broadcast__histories__title">ประวัติการ Broadcast</div>
        {renderBroadcastHistory()}
      </div>
    </div>
  )
}

export default SelectBroadcast
