import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Input, TextArea, Divider, Button } from 'semantic-ui-react'

import request from '../api'
import SelectableCardList from '../components/SelectableCardList.react'
import StickyBox from '../components/StickyBox.react'

import './Setting.css'

const DEFAULT_DELIVERY = [
  { _id: '1', value: 'ลงทะเบียน' },
  { _id: '2', value: 'EMS' },
  { _id: '3', value: 'Kerry' },
]

const Setting = () => {
  const [shopData, setShopData] = useState({
    name: '',
    description: '',
  })
  const [selectedItem, setSelectedItem] = useState([])
  const [register, setRegister] = useState('')
  const [ems, setEms] = useState('')
  const shop = useSelector((state) => state.shop)

  useEffect(() => {
    const fetchShopSetting = async () => {
      const { data } = await request.get(`/api/shop_setting/${shop.shop_id}`) // TODO: Promise.all[]
      await request.get(`/api/bank/${shop.shop_id}`) // TODO: No UI for create book bank

      setShopData({
        name: data.shop_name,
        description: data.shop_detail,
      })
    }
    fetchShopSetting()
  }, [])

  const onInputChange = (e) => {
    setShopData({
      ...shopData,
      [e.target.name]: e.target.value,
    })
  }

  const onGetSelectedItems = (items) => {
    setSelectedItem(items)
  }

  return (
    <div className="setting">
      <StickyBox title="การตั้งค่า" />
      <div className="setting__header">ข้อมูลร้านค้า</div>
      <div className="setting__form">
        <Input
          name="name"
          placeholder="ชื่อร้านค้า"
          onChange={onInputChange}
          value={shopData.name}
        />
        <TextArea
          name="description"
          placeholder="รายละเอียดเพิ่มเติมเกี่ยวกับร้านค้า"
          value={shopData.description}
          onChange={onInputChange}
        />
        <div className="setting__form__bank">
          <span>ธนาคารกสิกร</span>
          <Divider />
          <div className="setting__form__bank__detail">
            <span>ชื้อบัญชี</span>
            <Input name="bankName1" size="small" onChange={onInputChange} />
          </div>
          <div className="setting__form__bank__detail">
            <span>เลขบัญชี</span>
            <Input name="bankCode1" size="small" onChange={onInputChange} />
          </div>
        </div>
        <div className="setting__add-bank-button">
          <div className="setting__add-bank-button__plus">+</div>
          <div className="setting__add-bank-button__text">เพิ่มบัญชีธนาคาร</div>
          <div></div>
        </div>

        <div className="setting__first-form__delivery-options">
          <div className="setting__first-form__delivery-options__title">วิธีการจัดส่งสินค้า</div>
          <Divider />
          <div className="setting__first-form__delivery-options__options">
            <SelectableCardList
              contents={DEFAULT_DELIVERY}
              maxSelectable={DEFAULT_DELIVERY.length}
              onGetSelectedItems={onGetSelectedItems}
              multiple
            />
          </div>
          <div className="setting__first-form__delivery-options__input">
            <span>ค่าส่งสินค้าแบบลงทะเบียน</span>
            <Input
              type="number"
              name="shipping_register"
              value={register}
              onChange={(e) => setRegister(e.target.value)}
            />
          </div>
          <div className="setting__first-form__delivery-options__input">
            <span>ค่าส่งสินค้าแบบ EMS</span>
            <Input
              type="number"
              name="shipping_ems"
              value={ems}
              onChange={(e) => setEms(e.target.value)}
            />
          </div>
          <Divider />
          <div className="setting__first-form__delivery-options__note">
            ในกรณีที่ทางร้านค้าไม่เลือกวิธีการจัดส่งหรือไม่ใส่ค่าส่งสินค้า
            ระบบจะคำนวนเป็นการจัดส่งที่ทางร้านค้าเป็นคนออกค่าใช้จ่ายให้
          </div>
        </div>
      </div>
      <div className="setting__button">
        <Button onClick={() => null}>บันทึกการตั้งค่า</Button>
      </div>
    </div>
  )
}

// TODO: Click button and send request to request

export default Setting
