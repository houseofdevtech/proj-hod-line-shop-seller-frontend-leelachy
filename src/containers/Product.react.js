import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import _ from 'lodash'
import { Input, TextArea } from 'semantic-ui-react'

import BottomBar from '../components/BottomBar.react'
import DraftProductProperties from '../components/DraftProductProperties.react'
import ImageUploader from '../components/ImageUploader.react'
import ListItems from '../components/ListItems.react'
import Loading from '../components/Loading.react'
import ProductPropertiesPickerUploader from '../components/ProductPropertiesPickerUploader.react'
import StickyBox from '../components/StickyBox.react'
import Options from '../img/options.png'
import { getProducts, createProduct, editProducts, closedCreateProduct } from '../rest'
import { cartesian } from '../utils'

import './Product.css'

// Match the string start with # and includes [0-9A-F] 6 characters, insensitive
const regHexColor = /^#[0-9A-F]{6}$/i
const DEFAULT_PROPERTIES = {
  สี: [
    { value: 'ขาว', color_value: '', color_img_path: '', selected: false },
    { value: 'ดำ', color_value: '', color_img_path: '', selected: false },
  ],
  ขนาด: [
    { value: 'S', color_value: '', color_img_path: '', selected: false },
    { value: 'M', color_value: '', color_img_path: '', selected: false },
    { value: 'L', color_value: '', color_img_path: '', selected: false },
  ],
}
class Product extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      product: {
        product_code: '',
        name: '',
        images: [],
        detail: '',
        weight: '',
        price: '',
        qty: '',
        shipping_register: '',
        shipping_ems: '',
        base_delivery: '',
        add_delivery: '',
        other: [],
        data_item: [],
      },
      openMoreOption: false,
      openChargeOption: false,
      draftProperties: [],
      currentSelectedProperty: '',
      currentSelectedImagePosition: '',
      properties: DEFAULT_PROPERTIES,
      loading: true,
      productId: null,
      status: 'editing',
    }
  }

  async componentDidMount() {
    if (this.props.location.pathname === '/addProduct') {
      this.setState({ loading: false })
      return
    }

    let search = this.props.location.search
    let params = new URLSearchParams(search)
    let productId = params.get('product_id')

    this.setState({ productId })

    try {
      const { product_data } = await getProducts(this.props.user.chattyShopId, productId)
      console.log('===> ', product_data)
      this.setState({ loading: false })

      const colors = _.chain(product_data[0].product_items)
        .map((item) => item.color)
        .uniqBy((item) => item.color_name)
        .map((item) => ({
          value: item.color_name,
          color_value: item.color_value,
          color_img_path: item.color_img_path,
          selected: true,
        }))
        .value()
      console.log('product_data detail', product_data)

      console.log('product_data[0].product_items: ', product_data[0].product_items)
      const sizes = _.chain(product_data[0].product_items)
        .map((item) => item.size)
        .uniq()
        .map((item) => ({
          value: item,
          color_value: '',
          color_img_path: '',
          selected: true,
        }))
        .value()

      const unionColors = _.unionBy(colors, this.state.properties['สี'], (item) => item.value)
      const unionSizes = _.unionBy(sizes, this.state.properties['ขนาด'], (item) => item.value)
      const productImages = []

      if (!_.isEmpty(product_data[0].product_items)) {
        productImages.push({
          url: product_data[0].product_items[0].main_img_path,
          isCoverImage: true,
        })

        product_data[0].product_items[0].sub_img_path.forEach((item) => {
          productImages.push({ url: item.img_path, isCoverImage: false })
        })
      }

      this.setState({
        product: {
          ...this.state.product,
          product_id: product_data[0].product_id,
          product_code: product_data[0].product_code,
          name: product_data[0].product_name,
          data_item: product_data[0].product_items,
          detail: product_data[0].detail,
          shipping_ems: product_data[0].shipping_ems,
          shipping_register: product_data[0].shipping_register,
          base_delivery: _.isEmpty(product_data[0].base_delivery)
            ? 0
            : product_data[0].base_delivery,
          add_delivery: _.isEmpty(product_data[0].add_delivery) ? 0 : product_data[0].add_delivery,
          weight: product_data[0].weight,
          images: productImages,
          other: product_data[0].other || [],
          price: _.isEmpty(product_data[0].product_items[0])
            ? 0
            : product_data[0].product_items[0].price,
          qty: _.isEmpty(product_data[0].product_items) ? 0 : product_data[0].product_items[0].qty,
        },
        properties: {
          สี: unionColors,
          ขนาด: unionSizes,
        },
      })

      console.log('price', this.state.product.price)
    } catch (error) {
      console.error('Error:getProducts:', error)
    }
  }

  onImageUploadSelect = (images) => {
    this.setState({
      product: { ...this.state.product, images },
    })
  }

  onInputChange = (e) => {
    this.setState({
      product: { ...this.state.product, [e.target.name]: e.target.value },
    })
  }

  onToggleOptionForm = () => {
    this.setState((prevState) => ({
      openMoreOption: !prevState.openMoreOption,
    }))
  }

  onItemDelete = (key) => (propertyValue) => {
    const remainItems = _.reject(this.state.properties[key], (item) => item.value === propertyValue)
    this.setState((prevState) => ({
      properties: { ...prevState.properties, [key]: remainItems },
    }))
  }

  onProductSubmit = async () => {
    if (
      !(this.state.product.product_code && this.state.product.product_code.trim()) ||
      this.state.loading
    ) {
      return
    }

    const selectedColors = _.chain(this.state.properties['สี'])
      .filter((item) => item.selected)
      .map((item) => ({
        color: item.value,
        color_value: item.color_value,
        color_img_path: item.color_img_path,
      }))
      .value()

    const selectedSizes = _.chain(this.state.properties['ขนาด'])
      .filter((item) => item.selected)
      .map((item) => ({ size: item.value }))
      .value()

    const cartesianProducts = cartesian(selectedColors, selectedSizes) // [ [{A:1},{B:2}], [{C:3},{D:4}] ]

    const toArrayDataItems = _.map(cartesianProducts, (arrayItems) => {
      const result = {
        qty: _.toNumber(this.state.product.qty),
        price: _.toNumber(this.state.product.price),
      }
      _.forEach(arrayItems, (item) => {
        Object.assign(result, item)
      })

      return result
    })

    const data_item = _.map(toArrayDataItems, (item, index) => ({
      sku_code: this.state.product.product_code + '_' + (_.toNumber(index) + 1),
      ...item,
    }))

    const mainImagePath = !_.isEmpty(this.state.product.images) && this.state.product.images[0].url
    const excludeFirstImage = _.reject(this.state.product.images, (image, index) => index === 0)
    const imagesPath = excludeFirstImage.map((item) => item.url)
    const bodyData = {
      product_code: this.state.product.product_code,
      name: this.state.product.name,
      detail: this.state.product.detail,
      weight: _.toNumber(this.state.product.weight),
      shipping_register: _.toNumber(this.state.product.shipping_register),
      shipping_ems: _.toNumber(this.state.product.shipping_ems),
      base_delivery: _.toNumber(this.state.product.base_delivery),
      add_delivery: _.toNumber(this.state.product.add_delivery),
      ispassion: 1,
      data_item,
      other: this.state.product.other,
      path_gcs_name: 'products',
    }

    if (this.props.location.pathname !== '/addProduct') {
      Object.assign(bodyData, { product_id: _.toNumber(this.state.productId) })
    }

    let formdata = new FormData()

    const mainImage = this.state.product.images.find((item) => item.isCoverImage)
    formdata.append('main_img', mainImage.file)

    const subImages = _.chain(this.state.product.images)
      .filter((item) => !item.isCoverImage)
      .map((item) => item.file)
      .value()

    if (!_.isEmpty(subImages)) {
      subImages.forEach((item) => {
        formdata.append('sub_img', item)
      })
    }

    const colorImages = _.chain(this.state.properties['สี'])
      .filter((item) => item.selected)
      .map((item) => item.file)
      .value()

    if (!_.isEmpty(colorImages)) {
      colorImages.forEach((item, index) => {
        formdata.append('color_img', item, `${index}`)
      })
    }
    console.log('form', formdata)

    formdata.append('body_data', JSON.stringify(bodyData))

    try {
      this.setState({ status: 'submitting' })

      await closedCreateProduct(this.props.user.chattyShopId, formdata)

      this.props.history.goBack()
    } catch (error) {
      console.log('Error:onProductCreate', error)
    }

    this.setState({ status: 'editing' }) //
  }

  onMainImageSet = (images) => {
    this.setState({
      product: { ...this.state.product, images },
    })
  }

  onAddNewProperties = () => {
    const uniqueId = Math.random()
      .toString(36)
      .substr(2, 9)

    this.setState((prevState) => ({
      draftProperties: [
        ...prevState.draftProperties,
        {
          [uniqueId]: (
            <DraftProductProperties
              identity={uniqueId}
              onDelete={this.onDeleteDraftProperties}
              onSave={this.onNewPropertiesSave}
            />
          ),
        },
      ],
    }))
  }

  onDeleteDraftProperties = (identity) => {
    const newDraftProperties = this.state.draftProperties.filter(
      (item) => Object.keys(item)[0] !== identity,
    )

    this.setState({ draftProperties: newDraftProperties })
  }

  onNewPropertiesSave = (data) => {
    const subItems = _.toArray(data.draftSubItem)
    const items = subItems.map((item, index) => ({
      _id: index,
      value: item[index],
      color_value: '',
      color_img_path: '',
      selected: false,
    }))

    this.setState((prevState) => ({
      properties: { ...prevState.properties, [data.draftTitle]: items },
      draftProperties: [],
    }))
  }

  onDraftItemSave = (key, draftItems) => {
    const newItems = draftItems.map((item, index) => ({
      value: item,
      color_value: '',
      color_img_path: '',
      selected: false,
    }))

    this.setState((prevState) => ({
      properties: { ...prevState.properties, [key]: [...prevState.properties[key], ...newItems] },
    }))
  }

  onNavigateProductPage = () => {
    if (this.state.staus === 'submitting') {
      return
    }

    if (this.state.openMoreOption) {
      this.setState((prevState) => ({
        openMoreOption: !prevState.openMoreOption,
      }))
    } else {
      this.props.history.goBack()
    }
  }

  onProductPropertiesSet = (key) => (selectedItems = [], current, position) => {
    const values = selectedItems.map((item) => item.value)
    const subItems = this.state.properties[key].map((item) =>
      _.includes(values, item.value) ? { ...item, selected: true } : { ...item, selected: false },
    )

    const propertiesResult = { ...this.state.properties, [key]: subItems }

    if (!_.includes(values, current.value)) {
      this.setState({
        properties: propertiesResult,
        currentSelectedProperty: null,
      })
      return
    }

    this.setState({
      properties: propertiesResult,
      currentSelectedProperty: { key, data: current.value, position },
    })
  }

  onPropertyUploadedOrSelected = (data, file) => {
    const currentProperties = this.state.currentSelectedProperty.key

    const item = this.state.properties[currentProperties].find(
      (item) => item.value === this.state.currentSelectedProperty.data,
    )

    const result = regHexColor.test(data)
      ? { ...item, color_value: data, color_img_path: '' }
      : { ...item, color_value: '', color_img_path: data, file }

    const position = this.state.properties[currentProperties].findIndex(
      (item) => item.value === result.value,
    )

    const finalValue = { ...this.state.properties }
    finalValue[currentProperties][position] = result

    this.setState({
      properties: finalValue,
    })
  }

  renderPropertiesList = () => {
    const items = []
    for (const key in this.state.properties) {
      items.push(
        <ListItems
          key={key}
          title={key}
          currentSelected={this.state.properties[key].filter((item) => item.selected && item)}
          items={this.state.properties[key]}
          disabled={this.state.status === 'submitting'}
          onGetSelectedItems={this.onProductPropertiesSet(key)}
          onItemDelete={this.onItemDelete(key)}
          onDraftItemSave={this.onDraftItemSave}
        />,
      )
    }

    return items.map((item) => item)
  }

  renderOptionForm = () => {
    if (!this.state.openMoreOption) return

    return (
      <React.Fragment>
        <div className="product__option-form">
          {this.renderPropertiesList()}
          {this.renderAddMoreProperties()}
        </div>
      </React.Fragment>
    )
  }

  // TODO: Tmp remove
  // {_.isEmpty(this.state.draftProperties) && (
  //   <div className="product__more-properties" onClick={this.onAddNewProperties}>
  //     <div className="product__more-properties__add">+</div>
  //     <div className="product__more-properties-detail">
  //       <span>เพิ่มลักษณะตัวเลือกสินค้า</span>
  //       <span>เช่น เพืมลักษณะเนื้อผ้า คอเสื้อ หรือรายละเอียดอืนๆ</span>
  //     </div>
  //   </div>
  // )}

  renderFirstForm = () => {
    if (this.state.openMoreOption || this.state.openChargeOption) return
    const isSubmitting = this.state.status === 'submitting'

    return (
      <React.Fragment>
        <div className="product__image-uploader">
          <ImageUploader
            disabled={isSubmitting}
            images={this.state.product.images}
            onImageCoverSet={this.onMainImageSet}
            onImageUploadSelect={this.onImageUploadSelect}
          />
        </div>
        <div className="product__first-form">
          <div className="product__first-form__header">ข้อมูลสินค้า</div>
          <Input
            name="product_code"
            label="รหัสสินค้า"
            value={this.state.product.product_code}
            disabled={isSubmitting}
            onChange={this.onInputChange}
          />
          <Input
            name="name"
            value={this.state.product.name}
            label="ชื่อสินค้า"
            disabled={isSubmitting}
            onChange={this.onInputChange}
          />
          <div className="product_unit" id="baht">
            <Input
              name="price"
              value={this.state.product.price}
              label="ราคาสินค้า"
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
          <div className="product_unit" id="qty">
            <Input
              name="qty"
              value={this.state.product.qty}
              label="จำนวนสินค้าในสต็อก"
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
          <div className="product__first-form__button" onClick={this.onToggleOptionForm}>
            <div className="product__first-form__button__title">ตั้งค่าตัวเลือกสินค้าทั้งหมด</div>
            <img src={Options} alt="options" />
          </div>
          <TextArea
            className={classNames('product__first-form__text-area', { '--disabled': isSubmitting })}
            name="detail"
            rows={4}
            placeholder="ข้อความอธิบายสินค้า"
            value={this.state.product.detail}
            onChange={this.onInputChange}
          />
          <div className="product_unit" id="weight">
            <Input
              type="number"
              name="weight"
              label="น้ำหนักสินค้า"
              value={this.state.product.weight}
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
          <div className="product_unit" id="baht">
            <Input
              type="number"
              name="shipping_register"
              label="ราคาส่งลงทะเบียน"
              value={this.state.product.shipping_register}
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
          <div className="product_unit" id="baht">
            <Input
              type="number"
              name="shipping_ems"
              label="ราคาส่ง EMS"
              value={this.state.product.shipping_ems}
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
          <div className="product_unit" id="baht">
            <Input
              type="number"
              name="base_delivery"
              label="ราคาต่อชิ้น"
              value={this.state.product.base_delivery}
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
          <div className="product_unit" id="baht">
            <Input
              type="number"
              name="add_delivery"
              label="ราคาเพิ่มเติม"
              value={this.state.product.add_delivery}
              disabled={isSubmitting}
              onChange={this.onInputChange}
            />
          </div>
        </div>
      </React.Fragment>
    )
  }

  renderAddMoreProperties = () =>
    this.state.draftProperties.map((item, index) => <div key={index}>{Object.values(item)[0]}</div>)

  renderProductPropertiesPickerUploader = () => {
    if (
      !this.state.openMoreOption ||
      !this.state.currentSelectedProperty ||
      this.state.currentSelectedProperty.key === 'ขนาด'
    ) {
      return
    }

    return (
      <ProductPropertiesPickerUploader onPropertySelected={this.onPropertyUploadedOrSelected} />
    )
  }

  render() {
    let title
    if (this.state.openMoreOption) {
      title = 'ตัวเลือกสินค้า'
    } else if (this.state.openChargeOption) {
      title = 'ตั้งค่าการจัดส่งสินค้า'
    } else {
      title = 'รายละเอียดสินค้า'
    }

    const isSizesSelected = !_.isEmpty(this.state.properties['สี'].filter((item) => item.selected))
    const isColorsSelected = !_.isEmpty(
      this.state.properties['ขนาด'].filter((item) => item.selected),
    )
    const isFormReady =
      (this.state.product.weight &&
        isSizesSelected &&
        isColorsSelected &&
        !_.isEmpty(this.state.product.images)) ||
      this.state.loading

    if (this.state.loading) {
      return (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
          }}
        >
          <Loading />
        </div>
      )
    }

    console.log('product: ', this.state.product)
    console.log('properties: ', this.state.properties)
    return (
      <React.Fragment>
        <StickyBox title={title} backButtonDisable />
        <div className="product">
          {this.renderFirstForm()}
          {this.renderOptionForm()}
        </div>
        {this.renderProductPropertiesPickerUploader()}
        <BottomBar
          submitting={this.state.status === 'submitting'}
          buttonDisable={!isFormReady}
          onBack={this.onNavigateProductPage}
          onSubmit={this.onProductSubmit}
          buttonText={
            this.props.location.pathname === '/addProduct' ? 'เพิ่มสินค้า' : 'อัพเดทสินค้า'
          }
          backButtonDisable
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
})

export default connect(mapStateToProps)(Product)
