// import './Recommends.css';
// import _ from 'lodash'
// import React, { useState } from 'react';
// import { Button } from 'semantic-ui-react';

// import AddButtonCard from '../components/AddButtonCard.react';
// import StickyBox from '../components/StickyBox.react';
// import ProductBox from '../components/ProductBox.react';
// import SelectableCardList from '../components/SelectableCardList.react';

// import { MOCK_PRODUCTS } from '../mocks';

// const Recommends = props => {
//   const [selectedItems, setSelectedItems] = useState([]);

//   const renderProductBoxes = () => {
//     return MOCK_PRODUCTS.map((product, index) => (
//       <ProductBox product={product} count={index + 1} />
//     ));
//   };

//   const renderBanner = () => {
//     return (
//       <div className="recommends__banner">
//         กรุณาเลือกสินค้าเพื่อตั้งสินค้าแนะนำของร้านค้า
//       </div>
//     );
//   };
//   const onGetSelectedItems = items => {
//     setSelectedItems(items);
//   };
//   return (
//     <div className="recommends">
//       <StickyBox title="สินค้าแนะนำ" />
//       {renderBanner()}
//       <div className="recommends__products">
//         <AddButtonCard path="/addProduct" title="เพิ่มสินค้า" />
//         <SelectableCardList
//           contents={renderProductBoxes()}
//           maxSelectable={10}
//           onGetSelectedItems={onGetSelectedItems}
//           multiple
//         />
//       </div>
//       <Button
//         className="recommends__submit-button"
//         disabled={_.isEmpty(selectedItems)}
//         onClick={() => null} // TODO: Send data to  request
//       >
//         ยืนยันการเลือกสินค้า
//       </Button>
//     </div>
//   );
// };

// export default Recommends;
