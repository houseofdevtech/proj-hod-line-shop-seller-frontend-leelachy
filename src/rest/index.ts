import request from '../api'
import { BroadcastShop } from '../types/Broadcast'
import { Product, ProductOtherItem, ProductDataItem } from '../types/Product'
import { ProductGroupItem } from '../types/ProductGroup'
import { RecommendProduct } from '../types/RecommendProduct'

type ProductGroupBody = {
  products_group_id?: number
  name: string
  items: number[]
  path_gcs_name: string
}

type ProductPayload = {
  shop_id: string
  product_code: string
  name: string
  detail?: string
  weight: number
  shipping_register: number
  shipping_ems: number
  data_item: ProductDataItem[]
  other: ProductOtherItem[]
  main_img_path: string
  img_path: string[]
}

type ProductResponse = {
  product_group_id: number
  name: string
  main_img_path: string
  product_group_items: ProductGroupItem[]
}

// closeCreate
export const closedCreateProduct = async (shopId: string, payload) => {
  try {
    await request.post<{ message: string; id: string }>(
      `/api/product_upload/shop/${shopId}`,
      payload,
    )
  } catch (error) {
    throw new Error(error)
  }
}

// API09
export const createProduct = async (productPayload: ProductPayload) => {
  try {
    await request.post<{ message: string; id: string }>('/api/product', productPayload)
  } catch (error) {
    throw new Error(error)
  }
}

// API11
export const getProducts = async (shopId: string, productId: string) => {
  try {
    const { data } = await request.get<Product>(
      `/api/products/shop/${shopId}?product_id=${productId}`,
    )
    return data
  } catch (error) {
    throw new Error(error)
  }
}

// API12
export const editProducts = async (productId: string, productPayload: ProductPayload) => {
  try {
    await request.post<{ message: string; id: number }>(`/api/product/${productId}`, productPayload)
  } catch (error) {
    throw new Error(error)
  }
}

// API14
export const createRecommendedProduct = async (args: { shop_id: string; product_id: string[] }) => {
  try {
    await request.post<{ message: string; id: number }>('/api/recommended_product', args)
  } catch (error) {
    throw new Error(error)
  }
}

// API15
export const getRecommendedProducts = async () => {
  try {
    const { data } = await request.get<RecommendProduct[]>('/api/recommended_product')
    return data
  } catch (error) {
    throw new Error(error)
  }
}

// API17
export const createProductGroup = async (
  shopId: string,
  arg: {
    body_data: ProductGroupBody
    main_img_path: string
    path_gcs_name: string
  },
) => {
  try {
    await request.post<{ message: string; id: number }>(`/api/products_group/shop/${shopId}`, {
      body_data: arg.body_data,
      main_img_path: arg.main_img_path,
      path_gcs_name: arg.path_gcs_name,
    })
  } catch (error) {
    throw new Error(error)
  }
}

// API18/1
export const getProductGroupShop = async (shopId: string) => {
  try {
    const { data } = await request.get<{
      shop_id: string
      product_group_data: ProductResponse[]
    }>(`/api/products_group/shop/${shopId || 28}`)
    return data
  } catch (error) {
    throw new Error(error)
  }
}
// API19 NOTE: Got no product group when use this func
export const getProductsGroup = async (shopId: string) => {
  try {
    const { data } = await request.get<ProductResponse>(`/api/products_group/${shopId}`)
    return data
  } catch (error) {
    throw new Error(error)
  }
}

// API20
export const updateProductGroup = async (
  shopId: string,
  arg: { body_data: ProductGroupBody; main_img_path: string },
) => {
  try {
    await request.post<{ message: string }>(`/api/products_group/shop/${shopId}`, {
      body_data: arg.body_data,
      main_img_path: arg.main_img_path,
    })
  } catch (error) {
    throw new Error(error)
  }
}

// API21
export const deleteProductGroup = async (productGroupId: string) => {
  try {
    await request.delete(`/api/products_group/${productGroupId}`)
  } catch (error) {
    throw new Error(error)
  }
}

// API22 include edit broadcast
export const createBroadcast = async (
  shopId: string,
  arg: {
    body_data: {
      broadcast_code: string
      type: string
      items: number[]
      path_gcs_name: string
    }
    main_img_path: string
    path_gcs_name: string
  },
) => {
  try {
    request.post<{ message: string; id: number }>(`/api/broadcast/shop/${shopId}`, {
      body_data: arg.main_img_path,
      main_img_path: arg.main_img_path,
      path_gcs_name: arg.path_gcs_name,
    })
  } catch (error) {
    throw new Error(error)
  }
}

// API23/1
export const getBroadcastShop = async (shopId: string) => {
  try {
    const { data } = await request.get<BroadcastShop>(`/api/broadcast/shop/${shopId}?type=all`)
    return data
  } catch (error) {
    throw new Error(error)
  }
}
